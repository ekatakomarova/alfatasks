import java.io.File;
        import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;
        import java.util.Arrays;
        import java.util.Collections;

/*Создайте текстовый файл, в котором есть значения от 0 до 20 в произвольном
порядке (перемешаны). Значения указаны через запятую без пробелов.
● Прочитайте файл, отфильтруйте значения по возрастанию и выведите
результат в консоль.
● Прочитайте файл, отфильтруйте значения по убыванию и выведите результат в
консоль.*/

public class SecondTask
{
    private static Integer[] get_int()
    {
        Integer[] numbers; 

        try
        {
            Scanner scanner = new Scanner(new File("numbers.txt"));
            String[] substring = scanner.nextLine().split(",");
            scanner.close();

            numbers = new Integer[substring.length];

            for (int i = 0; i < substring.length; i++)
            {
                numbers[i] = Integer.parseInt(substring[i]);
            }
        }
        catch (FileNotFoundException ex)
        {
            System.out.print("No such file.");
            numbers = null;
        }
        catch (NoSuchElementException ex)
        {
            System.out.print("No elements in file.");
            numbers = null;
        }
        catch (NumberFormatException ex)
        {
            System.out.print("Wrong format of data in file.");
            numbers = null;
        }

        return numbers;
    }

    private static void print_arr(Integer[] arr)
    {
        for (Integer integer : arr)
        {
            System.out.print(integer + " ");
        }
    }

    public static void main(String[] args)
    {
        Integer[] numbers = get_int();

        if (numbers != null)
        {
            Arrays.sort(numbers);
            print_arr(numbers);

            System.out.print("\n");
            Arrays.sort(numbers, Collections.reverseOrder());
            print_arr(numbers);
        }
    }
}