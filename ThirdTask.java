/*Посчитайте факториал 20, и выведите результат в консоль.*/

public class ThirdTask
{
    public static void main(String[] args)
    {
        int base = 20;
        long res = 1;

        for (int i = 1; i <= base; i++)
        {
            res = res * i;
        }

        System.out.println(res);
    }
}

//Для вычисления значений более 20! (начиная с 21!) можно использовать BigInteger
//Указанное в задаче значение помещается в long.
